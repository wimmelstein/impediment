FROM adoptopenjdk:11-jre-hotspot
RUN mkdir /opt/app
EXPOSE 9002
COPY target/impediment-*-SNAPSHOT.jar /opt/app/japp.jar
COPY src/main/resources/example-xml/teamsandsprints.xml /tmp
CMD ["java", "-jar", "/opt/app/japp.jar"]