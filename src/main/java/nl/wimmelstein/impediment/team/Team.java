package nl.wimmelstein.impediment.team;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Team {

    private String teamName;
    private String description;

    @Override
    public String toString() {

        return "Het team heet " + this.getTeamName() + " en heeft als omschrijving: " + this.getDescription();
    }
}
