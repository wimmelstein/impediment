package nl.wimmelstein.impediment.controller;

import lombok.extern.java.Log;
import nl.wimmelstein.impediment.common.XMLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RequestMapping("/teams")
@Log
@RestController
public class TeamController {

    @Autowired
    private XMLService xmlService;

    @RequestMapping(value = "/populate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Map<String, List<Integer>> teams() {


        Map<String, List<Integer>> teamsList = xmlService.populateTeamsAndSprints();
        return teamsList;
    }
}
