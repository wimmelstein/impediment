package nl.wimmelstein.impediment.controller;

import lombok.extern.java.Log;
import nl.wimmelstein.impediment.common.FileService;
import nl.wimmelstein.impediment.entry.Entry;
import nl.wimmelstein.impediment.entry.EntryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Log
@RestController
@RequestMapping("/results")
public class ImpedimentController {

    @Autowired
    private FileService fileService;

    @Value("${BASE_DIR}")
    private String baseDir;

    @RequestMapping(value = "/total", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> calculateTotal(
            @RequestParam(value = "teamNaam") String teamNaam,
            @RequestParam(value = "sprint") String sprint) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        List<String> logLines = populateLogLines(teamNaam, sprint);
        String resultaat = String.valueOf(logLines.stream()
                .mapToInt(t -> Integer.parseInt(t.split(",")[8]))
                .sum());

        return new ResponseEntity<String>("{\"total\"" + ":" + "\"" + resultaat + "\"}", httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String giveAll(
            @RequestParam(value = "teamNaam") String teamNaam,
            @RequestParam(value = "sprint") String sprint) {

        List<String> logLines = populateLogLines(teamNaam, sprint);
        Collections.reverse(logLines);
        return logLines.stream()
                .limit(10)
                .collect(Collectors.joining("\n"));
    }

    @SuppressWarnings("Since15")
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ModelAndView writeNew(EntryDTO entryDTO,
                                 ModelMap model) throws IOException {



        Entry entry = new Entry();
        LocalDateTime localDateTime = LocalDateTime.now();
        String datum = localDateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")).toString();
        String tijd = localDateTime.format(DateTimeFormatter.ofPattern("hh:mm")).toString();

        entry.setImpedimentType(entryDTO.getImpedimentType());
        entry.setCorporateKey(entryDTO.getCorporateKey());
        entry.setHours(entryDTO.getHours());
        entry.setStory(entryDTO.getStory());
        entry.setOmschrijving(entryDTO.getOmschrijving());
        entry.setDatum(datum);
        entry.setTijd(tijd);
        entry.setTeamNaam(entryDTO.getTeamNaam());
        entry.setSprintNummer(entryDTO.getSprintNummer());
        fileService.writeToFile(entry.toString());
        return new ModelAndView("redirect:/", model);
    }


    private ArrayList<String> populateLogLines(String teamNaam, String sprint) {
        List<String> readLogLines = fileService.readLog(teamNaam, sprint);
        return (ArrayList<String>) readLogLines;
    }

    private String getHour(String line) {
        return line.split(",")[6];
    }

}