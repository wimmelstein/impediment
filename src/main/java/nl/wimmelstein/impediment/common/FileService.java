package nl.wimmelstein.impediment.common;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileService {


    private Config config;

    public FileService(Config config) {
        this.config = config;
    }

    public List readLog(String teamNaam, String sprint) {
        List<String> readLogLines = new ArrayList<>();

        try  (BufferedReader br = new BufferedReader(new FileReader(String.valueOf(config.getConfigFile())))) {
            readLogLines = br.lines()
                    .filter(t -> t.split(",")[3].equals(teamNaam))
                    .filter(t -> t.split(",")[4].equals(sprint))
                    .collect(Collectors.toList());

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return readLogLines;
    }

    public void writeToFile(String entry) throws IOException {

        File configFile = config.getConfigFile();
        try (FileWriter fw = new FileWriter(
                new File(String.valueOf(configFile)), true);
                BufferedWriter bw = new BufferedWriter(fw))
        {
            bw.append(entry + "\n");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
