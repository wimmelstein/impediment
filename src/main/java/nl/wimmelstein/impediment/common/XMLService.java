package nl.wimmelstein.impediment.common;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class XMLService {

    private Config config;

    public XMLService(Config config) {
        this.config = config;
    }

    public Map<String, List<Integer>> populateTeamsAndSprints() {

        Map<String, List<Integer>> teamsAndSprints = new HashMap<>();

        try {
            File file = new File(String.valueOf(config.getTeamFile()));
            System.out.println();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
                NodeList nodeListTeams = document.getElementsByTagName("team");
                    for (int i=0; i < nodeListTeams.getLength(); i++) {
                        Node node = nodeListTeams.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            String teamNaam = element.getElementsByTagName("naam").item(0).getTextContent();
                            List<Integer> listSprints = new ArrayList<>();
                            for (int j=0; j < element.getElementsByTagName("sprint").getLength(); j++) {
                                String sprint = element.getElementsByTagName("sprint").item(j).getTextContent();
                                listSprints.add(Integer.parseInt(sprint));
                            }
                            teamsAndSprints.put(teamNaam, listSprints);
                        }
                    }
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } finally {

            return teamsAndSprints;
        }
    }
}