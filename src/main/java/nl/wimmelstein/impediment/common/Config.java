package nl.wimmelstein.impediment.common;

import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class Config {

    private File configFile;
    private File teamFile;

    public File getTeamFile() {
        return teamFile;
    }

    public void setTeamFile(File teamFile) {
        this.teamFile = teamFile;
    }

    public File getConfigFile() {
        return configFile;
    }

    public void setConfigFile(File configFile) {
        this.configFile = configFile;
    }
}
