package nl.wimmelstein.impediment.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class ImpedimentRunner implements ApplicationRunner {
    private Config config;

    public ImpedimentRunner(Config config) {
        this.config = config;
    }

    @Value("${BASE_DIR}")
    private String baseDir;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        File configFile = new File(baseDir + File.separator + "impediment.csv");

        try {
            if (!configFile.exists()) configFile.createNewFile();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        config.setConfigFile(configFile);

        File teamFile = new File(baseDir + File.separator + "teamsandsprints.xml");

        config.setTeamFile(teamFile);
        System.out.println(configFile);

    }
}
