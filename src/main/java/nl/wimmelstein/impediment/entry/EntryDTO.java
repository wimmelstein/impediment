package nl.wimmelstein.impediment.entry;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntryDTO {

    String impedimentType;
    String corporateKey;
    String teamNaam;
    String sprintNummer;
    String omschrijving;
    String hours;
    String story;

}
