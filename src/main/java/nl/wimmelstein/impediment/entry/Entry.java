package nl.wimmelstein.impediment.entry;

import lombok.Getter;
import lombok.Setter;

import java.util.StringJoiner;

@Getter
@Setter
public class Entry extends EntryDTO{


    private String datum;
    private String tijd;

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(",");
            sj.add(impedimentType)
                    .add(datum)
                    .add(tijd)
                    .add(teamNaam)
                    .add(sprintNummer)
                    .add(corporateKey)
                    .add(story)
                    .add(omschrijving)
                    .add(hours);
        return sj.toString();
    }

}
