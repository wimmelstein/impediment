
$(document).ready(function () {

    var data = [];

    $.ajax({
        url: "teams/populate"
    }).then(function(json) {

        data = json;

        var teamOptions = '';
        var sprintOptions = '';

        var teamCookie = false;
        var sprintCookie = false;

        var cookies = [];
        cookies = document.cookie.split(";");
        if (cookies[0])
        {
            teamCookie = cookies[0].split("=")[1];
            sprintCookie = cookies[1].split("=")[1];
            console.log("Teamcookie: " + teamCookie);
            console.log("Sprintcookie: " + sprintCookie);
        }

        $.each(data, function(team, sprints) {
            teamOptions += '<option>' + team + '</option>';
        });

        $('#teams').html(teamOptions);

        if (teamCookie) setSelectedIndex(document.getElementById('teams'), teamCookie);

        $('#teams')
            .bind('change', function() {

                var sprintOptions = '';
                $.each(data[$('#teams').val()], function(index, sprint) {
                    sprintOptions += '<option>' + sprint + '</option>';
                });

                $('#sprints').html(sprintOptions);

                update($('#teams').val(), $('#sprints').val());
            });

        $.each(data[$('#teams').val()], function(index, sprint) {
            sprintOptions += '<option>' + sprint + '</option>';
        });

        $('#sprints').html(sprintOptions);

        if (sprintCookie) setSelectedIndex(document.getElementById('sprints'), sprintCookie);

        $('#sprints')
            .bind('change', function() {
                update($('#teams').val(), $('#sprints').val());
            });

        update($('#teams').val(), $('#sprints').val());
    });
})


function update(team, sprint) {
    bigNumber(team, sprint);
    fetchListOfImpediments(team, sprint);
    setTeamAndSprint(team, sprint);
    setCookie(team, sprint);
}

function bigNumber(team, sprint) {
    $.ajax({
        url: "results/total?teamNaam=" + team + "&sprint=" + sprint
    }).then(function (data) {
        $('.total').html(data.total);

    })
}

function fetchListOfImpediments(team, sprint) {
    $.ajax({
        url: "results/all?teamNaam=" + team + "&sprint=" + sprint
    }).then(function (data) {
        var lines = data.split("\n"),
            output = [],
            i;
        for (i = 0; i < lines.length; i++)
            output.push("<tr><td>"
                + lines[i].split(",").join("</td><td>")
                + "</td></tr>");
        output = "<table>" + output.join("") + "</table>";
        $('.all').html(output);

    });

}
function setTeamAndSprint(team, sprint) {
    $('#teamNaam').val(team);
    $('#sprintNummer').val(sprint);
    $('#teamDisplay').html(team);
    $('#sprintDisplay').html(sprint);
}

function setCookie(team, sprint) {

    console.log(window.location);
    var cookieStringTeam = "team=" + team;
    var cookieStringSprint = "sprint=" + sprint;
    document.cookie = cookieStringTeam;
    document.cookie = cookieStringSprint;
    console.log(document.cookie.split(";"));
}

function setSelectedIndex(dropDownBox, waarde) {

    for (var i=0; i<dropDownBox.options.length; i++) {
        if (dropDownBox.options.item(i).value === waarde) {
            dropDownBox.options.selectedIndex = i;
            return
        }
    }

}